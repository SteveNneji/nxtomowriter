NXtomoWriter
============
NXtomoWriter provides a function to convert tomography data stored as TIFF image files to a Nexus compliant file format 
(using [NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html))
  
How to run the code
-------------------
The code is Python 3 compatible. To run the source: 

1. Download the repository
2. Install dependencies
        
        pip install -r requirements.txt
3. Open python and type

        import nxtomowriter as ntw